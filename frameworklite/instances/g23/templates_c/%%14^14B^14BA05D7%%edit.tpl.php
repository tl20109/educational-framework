<?php /* Smarty version 2.6.14, created on 2015-05-20 10:37:37
         compiled from project/edit.tpl */ ?>
<?php echo $this->_tpl_vars['modules']['header']; ?>

 <link rel="stylesheet" href="<?php echo $this->_tpl_vars['url']['global']; ?>
js/jwysiwyg/jquery.wysiwyg.css" type="text/css" /> 
<!--   <script type="text/javascript" src="<?php echo $this->_tpl_vars['url']['global']; ?>
js/jquery/jquery-1.3.2.js"></script>  -->
  <script type="text/javascript" src="<?php echo $this->_tpl_vars['url']['global']; ?>
js/jwysiwyg/jquery.wysiwyg.js"></script> 
   <script src="<?php echo $this->_tpl_vars['url']['global']; ?>
js/editor.js"></script>
<script src="<?php echo $this->_tpl_vars['url']['global']; ?>
js/date.js"></script>
	<title>Edit - La Salle Social Travel</title>
 <div class="section">
     <img src="<?php echo $this->_tpl_vars['url']['global']; ?>
imag/project/section/edit.jpg" width="115" height="32" alt="Home" longdesc="Home - La Salle Social Travel" />
</div>
 <?php if ($this->_tpl_vars['sended'] == 'true'): ?>
			 <p><?php if ($this->_tpl_vars['inserted'] == 'true'): ?>
			 	Redirigir al post	 	
			 <?php else: ?>
			 	An Error occurred, please try again later. 
			 <?php endif; ?></p>
     <?php else: ?>   
         <?php if ($this->_tpl_vars['nohay'] == 'true'): ?>
         	You do not select a review to edit.
         <?php else: ?>
	         <?php if ($this->_tpl_vars['suya'] == 'false'): ?>
	         		This review is not yours.
	         <?php else: ?>
				 
				<div id="b_form">
			 	<form enctype="multipart/form-data"  action="" method="post">
			 	
				 	<fieldset>
				 		<legend>Basic Information</legend>
				 		
				 		<label for="title" class="float"><strong>Title:</strong></label><br />
						<input class="inp-text" name="title" id="title" type="text" size="30" value="<?php echo $this->_tpl_vars['data']['title']; ?>
" />
						<?php if ($this->_tpl_vars['title_error'] == 'true'): ?>
							<span class="err"><?php echo $this->_tpl_vars['title_msg']; ?>
</span>
						<?php endif; ?>
						<br />
						
						<label for="Country" class="float"><strong>Country:</strong></label><br />
						<input class="inp-text" name="country" id="country" type="text" size="30" value="<?php echo $this->_tpl_vars['data']['country']; ?>
" />
						<?php if ($this->_tpl_vars['country_error'] == 'true'): ?>
							<span  class="err"><?php echo $this->_tpl_vars['country_msg']; ?>
</span>
						<?php endif; ?>
						<br />
						
						<label for="Date" class="float"><strong>Date:</strong></label><br />
						<input class="inp-text" name="date" id="datepicker" type="text" size="30" value="<?php echo $this->_tpl_vars['data']['date']; ?>
" />
						<?php if ($this->_tpl_vars['date_error'] == 'true'): ?>
							<span  class="err"><?php echo $this->_tpl_vars['date_msg']; ?>
</span>
						<?php endif; ?>
						<br />

						<label for="Days" class="float"><strong>Days:</strong></label><br />
						<input class="inp-text" name="days" id="days" type="text" size="30" value="<?php echo $this->_tpl_vars['data']['days']; ?>
" />
						<?php if ($this->_tpl_vars['days_error'] == 'true'): ?>
							<span  class="err"><?php echo $this->_tpl_vars['days_msg']; ?>
</span>
						<?php endif; ?>
						<br />

						<label for="Hashtags" class="float"><strong>Hashtags:</strong></label><br />
						<input class="inp-text" name="hashtags" id="hashtags"   type="hidden" value="<?php echo $this->_tpl_vars['data']['hashtags']; ?>
">
            			<ul id="singleFieldTags"></ul>
           				<div id="events_container" class="err"></div>
						<?php if ($this->_tpl_vars['hashtags_error'] == 'true'): ?>
							<span  class="err"><?php echo $this->_tpl_vars['hashtags_msg']; ?>
</span>
						<?php endif; ?>
						<br />
												
						<label for="Qualification" class="float"><strong>Qualification:</strong></label><br />
						<select id="qualification" name="qualification">
									<?php if ($this->_tpl_vars['q1a'] == 'true'): ?>
								<option value="1" selected="selected"  >1</option>
							<?php else: ?>
								<option value="1"  >1</option>
							<?php endif; ?>
							<?php if ($this->_tpl_vars['q2a'] == 'true'): ?>
								<option value="2" selected="selected"  >2</option>
							<?php else: ?>
								<option value="2"  >2</option>
							<?php endif; ?>
							<?php if ($this->_tpl_vars['q3a'] == 'true'): ?>
								<option value="3" selected="selected"  >3</option>
							<?php else: ?>
								<option value="3"  >3</option>
							<?php endif; ?>
							<?php if ($this->_tpl_vars['q4a'] == 'true'): ?>
								<option value="4" selected="selected"  >4</option>
							<?php else: ?>
								<option value="4"  >4</option>
							<?php endif; ?>
							<?php if ($this->_tpl_vars['q5a'] == 'true'): ?>
								<option value="5" selected="selected"  >5</option>
							<?php else: ?>
								<option value="5"  >5</option>
							<?php endif; ?>
							<?php if ($this->_tpl_vars['q6a'] == 'true'): ?>
								<option value="6" selected="selected"  >6</option>
							<?php else: ?>
								<option value="6"  >6</option>
							<?php endif; ?>
							<?php if ($this->_tpl_vars['q7a'] == 'true'): ?>
								<option value="7" selected="selected"  >7</option>
							<?php else: ?>
								<option value="7"  >7</option>
							<?php endif; ?>
							<?php if ($this->_tpl_vars['q8a'] == 'true'): ?>
								<option value="8" selected="selected"  >8</option>
							<?php else: ?>
								<option value="8"  >8</option>
							<?php endif; ?>
							<?php if ($this->_tpl_vars['q9a'] == 'true'): ?>
								<option value="9" selected="selected"  >9</option>
							<?php else: ?>
								<option value="9"  >9</option>
							<?php endif; ?>
							<?php if ($this->_tpl_vars['q10a'] == 'true'): ?>
								<option value="10" selected="selected"  >10</option>
							<?php else: ?>
								<option value="10"  >10</option>
							<?php endif; ?>
							
						</select>
						<?php if ($this->_tpl_vars['qualification_error'] == 'true'): ?>
							<span  class="err"><?php echo $this->_tpl_vars['qualification_msg']; ?>
</span>
						<?php endif; ?>
						<br />
						
						<label for="Image Big" class="float"><strong>Image:</strong></label><br />
						<input class="files" type="file" name="imgbig">
						<?php if ($this->_tpl_vars['ri_error'] == 'true'): ?>
							<span  class="err"><?php echo $this->_tpl_vars['ri_msg']; ?>
</span>
						<?php endif; ?>
						<br /> 
						
						<input type="checkbox"  name="changeimg" checked="checked" style="display:none;" />
				 	</fieldset>
				 	<fieldset>
				 	    <legend>Description</legend>
				 		<textarea name="description" id="description" cols="30" rows="5"  ><?php echo $this->_tpl_vars['data']['description']; ?>
</textarea>
						<?php if ($this->_tpl_vars['description_error'] == 'true'): ?>
							<span  class="err"><?php echo $this->_tpl_vars['description_msg']; ?>
</span>
						<?php endif; ?>
				 	</fieldset>
				 	<p><input type="submit" value="enviar" /></p>
				 </form>
			 </div>
			<?php endif; ?>
		<?php endif; ?>
	<?php endif; ?>
 <?php echo $this->_tpl_vars['modules']['footer']; ?>