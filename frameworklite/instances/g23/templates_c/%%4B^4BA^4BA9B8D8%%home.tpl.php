<?php /* Smarty version 2.6.14, created on 2015-05-20 10:33:55
         compiled from project/home.tpl */ ?>
<?php echo $this->_tpl_vars['modules']['header']; ?>

<title>Home - LaSalle Social Travel</title>
<div class="section">
     <img src="<?php echo $this->_tpl_vars['url']['global']; ?>
imag/project/section/home.jpg" width="93" height="32" alt="Home" longdesc="Home - LaSalle Social Travel" />
</div>

	<h1>Last Review</h1>
	
	<p><h2><a href="<?php echo $this->_tpl_vars['url']['global']; ?>
review/<?php echo $this->_tpl_vars['review10'][0]['url']; ?>
"><?php echo $this->_tpl_vars['review10'][0]['title']; ?>
</a></h2>
	<strong>Hashtags:</strong>
	<?php $_from = $this->_tpl_vars['review10'][0]['hashtags']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['hashtag']):
?> 
	<a href="<?php echo $this->_tpl_vars['url']['global']; ?>
review/hash/<?php echo $this->_tpl_vars['hashtag']; ?>
">#<?php echo $this->_tpl_vars['hashtag']; ?>
</a>&nbsp;
	<?php endforeach; endif; unset($_from); ?>
	<br><br><br>
	<center><img src="<?php echo $this->_tpl_vars['url']['global']; ?>
imag/project/reviews/normal/<?php echo $this->_tpl_vars['review10'][0]['image']; ?>
"></center>
		
	<h1>Latest 10 Reviews</h1>
	<?php $_from = $this->_tpl_vars['review10']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['review']):
?> <!-- comprovar si te imatges o no -->
	<p style="clear: both;">
		<br><!-- ha de ser un link cap al visionat públic d’aquesta review -->
		<img src="<?php echo $this->_tpl_vars['url']['global']; ?>
imag/project/reviews/thumb/<?php echo $this->_tpl_vars['review']['image']; ?>
" style="float: left; margin: 0px 5px 5px 0px;" />
		<h3><a href="<?php echo $this->_tpl_vars['url']['global']; ?>
review/<?php echo $this->_tpl_vars['review']['url']; ?>
"><?php echo $this->_tpl_vars['review']['title']; ?>
</a></h3>			<!-- ha de ser un link cap al visionat públic d’aquesta review -->
		<?php echo $this->_tpl_vars['review']['description']; ?>
<br>		<!-- els 50 primers caracters de la review -->
		<strong>Date:</strong> <?php echo $this->_tpl_vars['review']['date']; ?>

		<strong>Qualification:</strong> <?php echo $this->_tpl_vars['review']['qualification']; ?>
</p>
	<?php endforeach; endif; unset($_from); ?>
	<br><br><br>
		
	<h1>Latest 10 Images</h1>
	<div style=" width:600px; margin: 0 auto">
	<?php $_from = $this->_tpl_vars['image10']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['image']):
?> <!-- comprovar si te imatges o no -->
		<img src="<?php echo $this->_tpl_vars['url']['global']; ?>
imag/project/reviews/thumb/<?php echo $this->_tpl_vars['image']['image']; ?>
" width="100" height="100" />
	<?php endforeach; endif; unset($_from); ?>
	</div>
	
<?php echo $this->_tpl_vars['modules']['footer']; ?>