<?php /* Smarty version 2.6.14, created on 2015-05-15 17:42:43
         compiled from project/register.tpl */ ?>
 <?php echo $this->_tpl_vars['modules']['header']; ?>

 <script src="<?php echo $this->_tpl_vars['url']['global']; ?>
js/date.js"></script>


 <title>Register - LaSalle Social Travel</title>
		
		<div class="section">
     <img src="<?php echo $this->_tpl_vars['url']['global']; ?>
imag/project/section/register.jpg" width="93" height="32" alt="Home" longdesc="Home - La Salle Social Travel" />
</div>
	<?php if ($this->_tpl_vars['sended'] == 'true'): ?>
			 <p><?php if ($this->_tpl_vars['inserted'] == 'true'): ?>
			 	<?php if ($this->_tpl_vars['dev_mode'] == 'false'): ?>
			 		MODE ONLINE<br />
			 		To activate account : <a href="<?php echo $this->_tpl_vars['link']; ?>
"><?php echo $this->_tpl_vars['link']; ?>
</a>
			 		
			 	<?php else: ?>
			 		An email has been sent to you current email address with the activation link of your account.
			 	<?php endif; ?>	 	
			 <?php else: ?>
			 	An Error occurred, please try again later. 
			 <?php endif; ?></p>
     <?php else: ?>   
   			 
		<div id="b_form"> 
			  <form action="" method="post">
				  <fieldset>
						<legend>Register New Member</legend>
							<label for="name" class="float"><strong>Name:</strong></label><br />
							<input class="inp-text" name="name" id="name" type="text" size="30" value="<?php echo $this->_tpl_vars['data']['name']; ?>
" />
							<?php if ($this->_tpl_vars['name_error'] == 'true'): ?>
								<span  class="err"><?php echo $this->_tpl_vars['name_msg']; ?>
</span>
							<?php endif; ?>
							<br />
			
							<label for="bday" class="float"><strong>Bday:</strong></label><br />
							<input class="inp-text" name="bday"  id="datepicker" type="text" size="30" value="<?php echo $this->_tpl_vars['data']['bday']; ?>
"/>
							<?php if ($this->_tpl_vars['bday_error'] == 'true'): ?>
								<span  class="err"><?php echo $this->_tpl_vars['bday_msg']; ?>
</span>
							<?php endif; ?>
							<br />
							
							<label for="email" class="float"><strong>eMail</strong></label><br />
							<input class="inp-text" name="email"  id="email" type="text" size="30" value="<?php echo $this->_tpl_vars['data']['email']; ?>
"/>
							<?php if ($this->_tpl_vars['email_error'] == 'true'): ?>
								<span  class="err"><?php echo $this->_tpl_vars['email_msg']; ?>
</span>
							<?php endif; ?>
							<br />
							
							<label for="password" class="float"><strong>Password</strong></label><br />
							<input class="inp-text" name="password"  id="password" type="password" size="30" value="<?php echo $this->_tpl_vars['data']['pwd']; ?>
"/>
				  			<?php if ($this->_tpl_vars['pass_error'] == 'true'): ?>
								<span  class="err"><?php echo $this->_tpl_vars['pass_msg']; ?>
</span>
							<?php endif; ?>
							<br />
				  </fieldset>
				  <p><input class="submit-button" type="submit" alt="Register" name="Submit" value="" /></p>
			  </form>
		</div>	 
			 
	<?php endif; ?>

 
 <?php echo $this->_tpl_vars['modules']['footer']; ?>