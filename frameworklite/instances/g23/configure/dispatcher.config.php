<?php
/*
 * Cada ruta SEO es mapeada a un controlador que se encuentra en el 'base.config.php'
 */

$config['default']				= 'HomeHomeController';
$config['home']					= 'HomeHomeController';

// Del projecte
$config['home']					= 'ProjectHomeController';
$config['register']				= 'ProjectRegisterController';
$config['login']				= 'ProjectLoginController';
$config['review']				= 'ProjectReviewController';
$config['search']				= 'ProjectSearchController';
$config['default']				= 'ProjectHomeController';


