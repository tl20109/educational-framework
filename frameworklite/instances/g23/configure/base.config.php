<?php
/*
 * Archivo de configuraci�n de las clases que usaremos
 * Se llama desde Configure::getClass('NombreClase');
 */

/**
 * Engine:  Don't modify.
 */
//$config['factory']						=  PATH_ENGINE . 'factory.class.php';
//$config['sql']							=  PATH_ENGINE . 'sql.class.php';


$config['mail']							=  PATH_ENGINE . 'mail.class.php';
$config['session']						=  PATH_ENGINE . 'session.class.php';
$config['user']							=  PATH_ENGINE . 'user.class.php';
$config['url']							=  PATH_ENGINE . 'url.class.php';
$config['uploader']						=  PATH_ENGINE . 'uploader.class.php';


$config['dispatcher']					=  PATH_ENGINE . 'dispatcher.class.php';


/** 
 * Controllers & Models
 *
 */

// 404 Controller
$config['ErrorError404Controller']		= PATH_CONTROLLERS . 'error/error404.ctrl.php';

// Home Controller
$config['HomeHomeController']			= PATH_CONTROLLERS . 'home/home.ctrl.php';

// Shared controllers
$config['SharedHeadController']			= PATH_CONTROLLERS . 'shared/head.ctrl.php';
$config['SharedFooterController']		= PATH_CONTROLLERS . 'shared/footer.ctrl.php';

/* Project controllers and models */
$config['ProjectHomeController'] 			= PATH_CONTROLLERS . 'project/home.ctrl.php';
$config['ProjectHeaderController'] 			= PATH_CONTROLLERS . 'project/header.ctrl.php';
$config['ProjectFooterController'] 			= PATH_CONTROLLERS . 'project/footer.ctrl.php';
$config['ProjectRegisterController'] 		= PATH_CONTROLLERS . 'project/register.ctrl.php';
$config['ProjectLoginController'] 			= PATH_CONTROLLERS . 'project/login.ctrl.php';
$config['ProjectReviewController'] 			= PATH_CONTROLLERS . 'project/review.ctrl.php';
$config['ProjectSearchController'] 			= PATH_CONTROLLERS . 'project/search.ctrl.php';

$config['ProjectHomeModel']					= PATH_MODELS . 'project/home.model.php';
$config['ProjectRegisterModel']				= PATH_MODELS . 'project/register.model.php';
$config['ProjectLoginModel']				= PATH_MODELS . 'project/login.model.php';
$config['ProjectReviewModel']				= PATH_MODELS . 'project/review.model.php';