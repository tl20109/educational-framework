  $(function(){
        	

        	var addEvent = function(text) {
				$('#events_container').html(text);
            };

           $('#singleFieldTags').tagit({
                // This will make Tag-it submit a single form value, as a comma-delimited field.
                singleField: true,
                singleFieldNode: $('#hashtags'),
                tagLimit: 5,
                onTagLimitExceeded: function(evt, ui) {
                    addEvent('Only 5 tags are allowed');
                },
                beforeTagAdded : function(evt, ui) {
                    ui.tag.prepend("#");
                }
            });


            
 });