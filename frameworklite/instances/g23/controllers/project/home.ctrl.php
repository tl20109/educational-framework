<?php

class ProjectHomeController extends Controller
{
	public function build( )
	{

		$this->setLayout( 'project/home.tpl' );
		$model 	= $this->getClass( 'ProjectHomeModel' );
		$image=$model->get10Images();
		$review=$model->getLast10Review();

		
		foreach($review as $entry)
		{
			$entry['description'] = substr( strip_tags($entry['description']) ,0,50)."...";
			$entry['hashtags'] =explode(',',$entry['hashtags']);
			//$hash_array = explode(',',$entry['hashtags']);
			//var_dump($hash_array);
			//$entry['hashtags'] = implode(',',array_map(function($value){ return '#'.$value; },$hash_array));

			$reviews[] = $entry;
		}
		
		if(!empty($image))
			$this->assign('image10',$image);
		if(!empty($reviews))
			$this->assign('review10',$reviews); 
	}


	public function loadModules()
	{
		$modules['header']	= 'ProjectHeaderController';
		$modules['footer']	= 'ProjectFooterController';

		return $modules;
	}
}
?>