<?php

class ProjectHeaderController extends Controller
{

	public function build( )
	{
		
		$session = Session::getInstance();
		
		$this->setLayout( 'project/header.tpl' );
	
		if($session->get('logged') == true ){
			$this->assign('logged','true');
			$this->assign('id',$session->get('id'));
			$this->assign('name',$session->get('name'));
		}

		
		
	}
}


?>
