<?php

class ProjectSearchController extends Controller
{
	public function build( )
	{
		
		$session = Session::getInstance();
		if( $session->get('logged') == true)
		{
			$sid = $session->get('id');

			$this->setLayout("project/search.tpl");
			if( Filter::isSent('search') )
			{
				$search = Filter::getString('search');
				//var_dump($search);
				if ($search == "")
				{
					$this->assign('vacio','true');
				}
				else 
				{
					$this->assign('vacio','false');
					$model = $this->getClass( 'ProjectReviewModel' );
					$review = $model->searchFollowers($search, $sid);
					//var_dump($review);
					//print_r($review);
					if( empty($review) )
					{
						$this->assign('nohay','true');
						//no hi ha reviews que coincideixin
					}
					else
					{
						$this->assign('reviews',$review);				
					}
				}
			} 
		}else{

			header('Location: '.URL_ABSOLUTE.'login');	
		}
	}


	public function loadModules()
	{
		$modules['header']	= 'ProjectHeaderController';
		$modules['footer']	= 'ProjectFooterController';

		return $modules;
	}
		
}
?>
	