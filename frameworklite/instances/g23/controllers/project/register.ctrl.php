<?php

class ProjectRegisterController extends Controller
{
	public function build( )
	{
		
		$params = $this->getParams();
		$model 	= $this->getClass( 'ProjectRegisterModel' );
		
		if(isset($params['url_arguments'][0]) && $params['url_arguments'][0] ==  "active")
		{
				
				$this->setLayout( 'project/active.tpl' );
				$hash = $params['url_arguments'][1];
				$isActive = $model->checkActive($hash);
				//var_dump($isActive);
				if( !empty($isActive) )
				{
					$this->assign('ValidHash','true');
					if($isActive[0]['active'] == '1'){
						$this->assign('isActivated','true');
					} else {
						
							$model->changeActive($hash);	
							$user = $model->getUser($hash);	
							//var_dump($user);
							$session =  Session::getInstance();		
							$session->set($user[0]);
							$session->set('logged',true);							
//							echo "entra2";
							$this->assign('isActivated','false');
//							sleep(5);					
							header('refresh: 5; url='.URL_ABSOLUTE.'home');	
						//	header('Location: '.URL_ABSOLUTE.'home');
						//echo "change";
					}
				} else {
					$this->assign('ValidHash','false');
				}
		} else 
		{
			
				$this->setLayout( 'project/register.tpl' );
				$this->assign('sended','false');
				if ( Filter::isSent( 'name' ) && Filter::isSent( 'bday' ) && Filter::isSent( 'email' ) && Filter::isSent( 'password' ) )
				{
					$error= false;
		//			var_dump(Filter::getString( 'name' ));
					$name = Filter::getString( 'name' );
		//			var_dump($name);
					if ($name == "" || strlen($name)<4){
						$error= true;
						$error_name="Write a valid Name.";
						$this->assign( 'name_error', 'true' );
						$this->assign( 'name_msg', $error_name );
					}else{
						$content_name 	= $model->checkName($name);
						if(!empty($content_name[0])){
							$error= true;
							$error_name="Name already exists";
							$this->assign( 'name_error', 'true' );
							$this->assign( 'name_msg', $error_name );			
						}
					}
					
					$error_bday= 0;
					$bday = Filter::getString( 'bday' );
				
					if ( $bday == "" || !$this->testcadena($bday) )
					{
							$error = true;
							$error_bday="Write a valid date(dd/mm/yyyy).";
							$this->assign( 'bday_error', 'true' );
							$this->assign( 'bday_msg', $error_bday );
					}else{
						
						//Partim la data per saber el dia, mes i any
						$bday_tmp = explode("/", $bday);
						//Treurem l'edat
						$age = (date("md", date("U", mktime(0, 0, 0, $bday_tmp[1], $bday_tmp[0], $bday_tmp[2]))) > date("md") 
							? ((date("Y") - $bday_tmp[2]) - 1)
						    : (date("Y") - $bday_tmp[2]));
					
						if($age < 18){
							$error= true;
							$error_bday="Access denied for people under 18 years.";
							$this->assign( 'bday_error', 'true' );
							$this->assign( 'bday_msg', $error_bday );			
						}
					}
					
					$email = Filter::getString( 'email' );
					if ($email == ""){
						$error= true;
						$error_email="Write a valid eMail.";
						$this->assign( 'email_error', 'true' );
						$this->assign( 'email_msg', $error_email );
					}else{
						$content_email = $model ->checkEmail($email);
						if(!empty($content_email[0])){
							$error= true;
							$error_email="Email already exists";
							$this->assign( 'email_error', 'true' );
							$this->assign( 'email_msg', $error_email );
						}
					}

					$pass = Filter::getString( 'password' );
					if ($pass == ""){ 
						$error= true;
						$error_password="Write a Password.";
						$this->assign( 'pass_error', 'true' );
						$this->assign( 'pass_msg', $error_password );
					}else{
						if(strlen($pass)<6 || strlen($pass)>20){
							$error= true;
							$error_pass = "Password ERROR: length between 6 and 20";
							$this->assign( 'pass_error', 'true' );
							$this->assign( 'pass_msg', $error_pass );
						}
					}
				
					if($error === false){
						$this->assign('sended','true');
//						echo"entra";
						$r_hash = $name."_/·2f".$email;
						$hash = md5($r_hash);						
						$subject = "La Salle Social Travel - Active Account";						
						$link = URL_ABSOLUTE.'register/active/'.$hash;
						
							$to = $email;
							$content = 'Active Account: <a href=\"'.$link.'\">'.$link.'</a>';
							$subject = 'La Salle Social Travel - Administration';
							$from = 'tl20109@salleurl.edu';

							$uri = 'https://mandrillapp.com/api/1.0/messages/send.json';
							$api_key = 'YBKMvgMmdArZ8ELiJoqHhA';
							$content_text = strip_tags($content);

							$postString = '{
							"key": "' . $api_key . '",
							"message": {
							 "html": "' . $content . '",
							 "text": "' . $content_text . '",
							 "subject": "' . $subject . '",
							 "from_email": "' . $from . '",
							 "from_name": "' . $from . '",
							 "to": [
							 {
							 "email": "' . $to . '",
							 "name": "' . $to . '"
							 }
							 ],
							 "track_opens": true,
							 "track_clicks": true,
							 "auto_text": true,
							 "url_strip_qs": true,
							 "preserve_recipients": true
							},
							"async": false
							}';

							$ch = curl_init();
							curl_setopt($ch, CURLOPT_URL, $uri);
							curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true );
							curl_setopt($ch, CURLOPT_RETURNTRANSFER, true );
							curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
							curl_setopt($ch, CURLOPT_POST, true);
							curl_setopt($ch, CURLOPT_POSTFIELDS, $postString);
							//$result = curl_exec($ch);
							try{
								// $send = $mail->sendMail($email,$subject,'<a href="'.$link.'">Active Account:'.$link.'</a>');
								$send = curl_exec($ch);
							} catch(Exception $e){
								echo $e->getMessage();
							}
							if($send)
							{
								$this->assign('inserted','true');
								$data['name'] = $name;
								$data['email'] = $email;
								$data['bday'] = $bday;
								$data['password'] = md5($pass);
								$data['hash'] = $hash;
								$model->insertUser($data);
							} else {
								$this->assign('inserted','false');
							}
											
					} else {
							$data['name'] = $name;
							$data['bday'] = $bday;
							$data['email'] = $email;
							$data['pwd'] = $pass;					

							$this->assign('data',$data);
					}
				}
		}
	}
	
	// L'utilitzarem per a fer la comprovació de les cadenes.
	private function testcadena($cadena)
	{
		if (strlen($cadena)!=10){ 
//			echo "error lenth";		   
		    return false; 
		} 
		
		$num = "0123456789"; 
		for ($i=0; $i<2; $i++){ 
		     if (strpos($num, substr($cadena,$i,1))===false){ 
//		         echo "error num1";         
		         return false; 
		      } 
		}

		$barra = "/";
		for ($i=2; $i<3; $i++){ 
		      if (strpos($barra, substr($cadena,$i,1))===false){ 
//		         echo "error format1";
		         return false; 
		      } 
		}

		$num = "0123456789";
		for ($i=3; $i<5; $i++){ 
		      if (strpos($num, substr($cadena,$i,1))===false){ 
//		         echo "erro num2";         
		         return false; 
		      } 
		}


		$barra = "/";
		for ($i=5; $i<6; $i++){ 
		      if (strpos($barra, substr($cadena,$i,1))===false){ 
//		         echo "error format2";
		         return false; 
		      } 
		}

		$num = "0123456789";
		for ($i=6; $i<10; $i++){ 
		      if (strpos($num, substr($cadena,$i,1))===false){ 
//		         echo "erro num3";         
		         return false; 
		      } 
		}
		return true; 
	}

	public function loadModules()
	{
		$modules['header']	= 'ProjectHeaderController';
		$modules['footer']	= 'ProjectFooterController';

		return $modules;
	}
}
?>