<?php

class ProjectReviewController extends Controller
{

	private function seoUrl($string) {
	    //Unwanted:  {UPPERCASE} ; / ? : @ & = + $ , . ! ~ * ' ( )
	    $string = strtolower($string);
	
	    //Strip any unwanted characters
	    $string = preg_replace("/[^a-z0-9_\s-]/", "", $string);

	    //Clean multiple dashes or whitespaces
	   $string = preg_replace("/[\s-]+/", " ", $string);
	
	    //Convert whitespaces and underscore to dash
	   $string = preg_replace("/[\s_]/", "-", $string);
	  //   var_dump($string);
	   return $string;
	}  
	
	public function build( )
	{
	//	var_dump($_FILES);	
		$params = $this->getParams();
		

		
		if(isset($params['url_arguments'][0]) && $params['url_arguments'][0] ==  "manage")
		{
				$session = Session::getInstance();
				if( $session->get('logged') == true)
				{
					$model = $this->getClass('ProjectReviewModel');
					$id = $session->get('id');
					$total = $model->getNumberByUser($id);
					
					$this->setLayout( 'project/manage.tpl' );
					//var_dump($total);
					
					if($total[0]["total"] == 0)
					{
						$this->assign('vacio','true');
					}
					else
					{
						$this->assign('vacio','false');
						
						//sense paginacio		
						//$reviews = $model->getByUser();
						//$this->assign('reviews',$reviews);
						
						//amb paginacio
						
						if(!isset($params['url_arguments'][1]) || empty($params['url_arguments'][1]) )
						{
							$page = 1;
						}
						else
						{
							$page = $params['url_arguments'][1];
						}
			
						$max = 10;
						$total_pages = ceil($total[0]['total'] / $max);
			
			
				
						if( $page > $total_pages || $page < 0)
						{
							$this->setLayout( 'project/error.tpl' );
							$this->assign('error','You are trying to access into a web page with no reviews.');
						}
						else 
						{
							$start = ($page * $max) - $max;
							$reviews = $model->getByUser($session->get('id'),$start,$max);
						//	var_dump($reviews);
							$this->assign('reviews',$reviews);
				
							if($page == 1)
							{
								$this->assign('prev','false');
							}
							else
							{
								$this->assign('prev','true');
								$this->assign('a_prev',$page-1);
							}
								
							if($page == $total_pages)
							{
								$this->assign('next','false');
							}
							else
							{
								$this->assign('next','true');
								$this->assign('a_next',$page+1);
							}
						}
			
				
					}
				}
				else
				{
					header('HTTP/1.1 403 Forbidden');
					$this->setLayout('project/error403.tpl');
				
				}
		}
		else if(isset($params['url_arguments'][0]) && $params['url_arguments'][0] ==  "edit")
		{
				$session = Session::getInstance();
				if( $session->get('logged') != true)
				{
					header('HTTP/1.1 403 Forbidden');
					$this->setLayout('project/error403.tpl');
				}
				else
				{
					$this->setLayout( 'project/edit.tpl' );
			
					if(!isset($params['url_arguments'][1]) || empty($params['url_arguments'][1]) )
					{
						$this->assign('nohay','true');
					}
					else
					{
						$this->assign('nohay','false');	
						$model = $this->getClass('ProjectReviewModel');
						$id = $params['url_arguments'][1];	
						$suya = $model->isYours($id,$session->get('id'));
						if( !empty($suya) )
						{
							$this->assign('suya','true');
							$error = $this->checkInfo();
						
							if($error == true)
							{	
										
								$review = $model->getReview($id);
								$review = $suya;
								if(!Filter::isSent('title'))
								{
									$this->assign('q'.$review[0]["qualification"].'a','true');
									$this->assign('data',$review[0]);
								}
							}
							else
							{
								if(Filter::isSent('changeimg'))
								{
									$ext = strrchr($_FILES["imgbig"]["name"],'.');  
									$_FILES["imgbig"]["name"] = time().$ext;
									//$uploader = new Uploader();
									echo $_SERVER['DOCUMENT_ROOT'].'/imag/project/reviews/normal/';
							
									$sube = move_uploaded_file($_FILES["imgbig"]["tmp_name"],$_SERVER['DOCUMENT_ROOT'].'/imag/project/reviews/normal/'.$_FILES["imgbig"]["name"]);	
									if($sube)
									{
										$this->generateThumb( $_FILES["imgbig"]["name"]);
										$this->generateThumbBig( $_FILES["imgbig"]["name"]);
										$model->updateImage($id);
										$model->insertImage($_FILES["imgbig"]["name"], $id);
									}
									else 
									{
										//echo "error al pujar la fotografia";
									}
								}
								
								
								$data['title'] = Filter::getString( 'title' );
								$data['description'] = Filter::getString( 'description' );
								$data['country'] = Filter::getString( 'country' );
								$data['days'] = Filter::getString( 'days' );
								$data['date'] = Filter::getString( 'date' );
								$data['qualification'] = Filter::getString( 'qualification' );
								$data['hashtags'] = Filter::getString( 'hashtags' );
								$model->updateReview($id,$data);
								$url_anterior = $model->getURL($id);
								$str = $id.Filter::getString('title');
								$url_nueva = $this->seoUrl( $str  );
								if( strcmp($url_anterior[0]['url'],$url_nueva) != 0)
								{
									$id_nueva = $model->insertURL($url_nueva,$id);
									$model->updateURL($id,$id_nueva);
								}								
								
								header('Location:'.URL_ABSOLUTE.'review/'.$url_nueva);
							}
						}
						else
						{
							$this->assign('suya','false');
						}
					}
				}
		} else if(isset($params['url_arguments'][0]) && $params['url_arguments'][0] ==  "delete")
		{
				$session = Session::getInstance();
				if( $session->get('logged') != true)
				{
					header('HTTP/1.1 403 Forbidden');
					$this->setLayout('project/error403.tpl');
				}
				else
				{
					$this->setLayout('project/delete.tpl');
					if(!isset($params['url_arguments'][1]) || empty($params['url_arguments'][1]) )
					{
						$this->assign('nohay','true');
					}
					else
					{
						$this->assign('nohay','false');
						
						
							
								$model = $this->getClass('ProjectReviewModel');
								$id = $params['url_arguments'][1];	
								$suya = $model->isYours($id,$session->get('id'));
								//var_dump($suya);
								if( !empty($suya) )
								{
									$this->assign('suya','true');
									if( Filter::isSent('yes') )
									{
										$model->deleteReview($suya[0]['id']);
										header('Location: '.URL_ABSOLUTE.'review/manage');
									} 
									if( Filter::isSent('no') )
									{
										header('Location: '.URL_ABSOLUTE.'review/manage');
									}
								}
								else
								{
									$this->assign('suya','false');
								}
						
					}
				}
		} else if(isset($params['url_arguments'][0]) && $params['url_arguments'][0] == "add")
		{
			
				$session =  Session::getInstance();

				if($session->get('logged') == true ){
											
						$this->setLayout( 'project/addreview.tpl' );
						$this->assign('q5a','true');	
						$this->assign('sended','false');
						
						$error = $this->checkInfo();
						//var_dump( $error );
						//exit();
						if($error === false){
								
							$ext = strrchr($_FILES["imgbig"]["name"],'.');  
							$_FILES["imgbig"]["name"] = time().$ext;
							//$uploader = new Uploader();
							//echo $_SERVER['DOCUMENT_ROOT'].'/imag/project/reviews/normal/';
							//$uploader->setInitVars($_SERVER['DOCUMENT_ROOT'].'imag/project/reviews/normal/','imgbig');
							//$uploader->upload();
							$sube = move_uploaded_file($_FILES["imgbig"]["tmp_name"],$_SERVER['DOCUMENT_ROOT'].'/imag/project/reviews/normal/'.$_FILES["imgbig"]["name"]);
							//var_dump( $sube );
							//echo $_SERVER['DOCUMENT_ROOT'].'/imag/project/reviews/normal/'.$_FILES["imgbig"]["name"];
							if( $sube)
							{					
								$this->assign('sended','true');
								$this->assign('inserted','true');
								$data['title'] = Filter::getString( 'title' );
								$data['id_user'] = $session->get('id');
								$data['description'] = Filter::getString( 'description' );
								$data['country'] = Filter::getString( 'country' );
								$data['days'] = Filter::getString( 'days' );
								$data['date'] = Filter::getString( 'date' );
								$data['qualification'] = Filter::getString( 'qualification' );
								$data['hashtags'] = Filter::getString( 'hashtags' );
								$model 	= $this->getClass( 'ProjectReviewModel' );
								$id = $model->insertReview($data);
								//var_dump($id);
								//falta incloure les imatges
								$string = $id.$data['title'];
								$url = $this->seoUrl($string);
								//var_dump($data['description']);
								$model->insertURL($url,$id);
								
								$this->generateThumb( $_FILES["imgbig"]["name"]);
								$this->generateThumbBig( $_FILES["imgbig"]["name"]);
								
								$model->insertImage($_FILES["imgbig"]["name"], $id);
							}
						
							 //var_dump($uploader->getFileName());
							header('Location: '.URL_ABSOLUTE.'review/'.$url);
						} 						
						
				}
				else{
					header('HTTP/1.1 403 Forbidden');
					$this->setLayout('project/error403.tpl');		
				}
		} 
		else if(isset($params['url_arguments'][0]) && $params['url_arguments'][0] == "follow")
		{
				$session = Session::getInstance();
				if( $session->get('logged') == true)
				{
					$model = $this->getClass('ProjectReviewModel');
					$id = $session->get('id');
					$total = $model->getFollowers($id);

					if ( isset($params['url_arguments'][1]) && !empty($params['url_arguments'][1]) ){
							$model->follow($params['url_arguments'][1],$id);
					}
					
					$this->setLayout( 'project/follow.tpl' );
					//var_dump($total);
					
					if(empty($total))
					{
						$this->assign('vacio','true');
					}
					else
					{
						$this->assign('vacio','false');
						
						//sense paginacio		
						//$reviews = $model->getByUser();
						//$this->assign('reviews',$reviews);
						
						//amb paginacio
						
						if(!isset($params['url_arguments'][1]) || empty($params['url_arguments'][1]) )
						{
							$page = 1;
						}
						else
						{
							$page = $params['url_arguments'][1];
						}
			
						$max = 10;
						$total_pages = ceil(count($total) / $max);
			
			
				
						if( $page > $total_pages || $page < 0)
						{
							$this->setLayout( 'project/error.tpl' );
							$this->assign('error','You are trying to access into a web page with no data.');
						}
						else 
						{
							$start = ($page * $max) - $max;
							$this->assign('reviews',$total);
				
							if($page == 1)
							{
								$this->assign('prev','false');
							}
							else
							{
								$this->assign('prev','true');
								$this->assign('a_prev',$page-1);
							}
								
							if($page == $total_pages)
							{
								$this->assign('next','false');
							}
							else
							{
								$this->assign('next','true');
								$this->assign('a_next',$page+1);
							}
						}
			
				
					}
				}
				else
				{
					header('HTTP/1.1 403 Forbidden');
					$this->setLayout('project/error403.tpl');
				
				}		
		}else if(isset($params['url_arguments'][0]) && $params['url_arguments'][0] ==  "unfollow")
		{
				$session = Session::getInstance();
				$sid = $session->get('id');
				if( $session->get('logged') != true)
				{
					header('HTTP/1.1 403 Forbidden');
					$this->setLayout('project/error403.tpl');
				}
				else
				{
					$this->setLayout('project/unfollow.tpl');
					if(!isset($params['url_arguments'][1]) || empty($params['url_arguments'][1]) )
					{
						$this->assign('nohay','true');
					}
					else
					{
						$this->assign('nohay','false');
													
						$model = $this->getClass('ProjectReviewModel');
						$id = $params['url_arguments'][1];	
						$model->unfollow($id, $sid);
						//var_dump($total);
						header('Location: '.URL_ABSOLUTE.'review/follow');
					}
				}
		}else if(isset($params['url_arguments'][0]) && $params['url_arguments'][0] == "hash")
		{
				if( empty($params['url_arguments'][1]) ){
					$error=true;
					$this->setLayout( 'project/error.tpl' );
					$error_noarguments="There's no hashtag selected.";
					$this->assign( 'error', $error_noarguments );
				}
				else 
				{
					$this->setLayout("project/hashsearch.tpl");
					$params = $this->getParams();
					$search = $params['url_arguments'][1];
					if(!empty($search))
					{

						//var_dump($search);
					
						$this->assign('vacio','false');
						$model = $this->getClass( 'ProjectReviewModel' );
						$review = $model->searchReview($search);
				
						//print_r($review);
						if( empty($review) )
						{
							$this->assign('nohay','true');
							//no hi ha reviews que coincideixin
						}
						else
						{
							for ($i=0;$i<count($review);$i++)
							{
							
								$description=$review[$i]['description'];
								if (strlen($description)>= 50)
								{
									$texto=substr(strip_tags($description),0,50)."...";
									$review[$i]['description']=$texto;
								}
							}
							//var_dump($review);
							$this->assign('reviews',$review);				
						}
			
					} else {

						$this->assign('vacio','true');
					}
				}
		}else
		{
				if( empty($params['url_arguments'][0]) ){
					$error=true;
					$this->setLayout( 'project/error.tpl' );
					$error_noarguments="There's no review selected.";
					$this->assign( 'error', $error_noarguments );
				}
				else 
				{
					$id = $this->getIdOfReview($params['url_arguments'][0]);
					$model 	= $this->getClass( 'ProjectReviewModel' );
					$this->setLayout( 'project/viewreview.tpl' );
					$infoReview = $model->getReview((int)$id);
					
					if(!empty($infoReview))
					{

						$count = $model->getCountByUser($infoReview[0]['id_user']);
			
						$infoReview[0]['hashtags'] = explode(',',$infoReview[0]['hashtags']);
						$infoReview[0]['total'] = $count[0]["total"];
						//$hash_array = explode(',',$infoReview[0]['hashtags']);
						//var_dump($hash_array);
						//$infoReview[0]['hashtags'] = implode(',',array_map(function($value){ return '#'.$value; },$hash_array));
						//exit();
						$this->assign('found','true');
						$this->setLayout( 'project/viewreview.tpl' );
						$this->assign('data',$infoReview[0]);
					}
					 else
					{
						$this->assign('found','false');
					}
				}				
		}	
	}
	
	private function generateThumb($name)
	{
		$image = new Images();
		
		$from = $_SERVER['DOCUMENT_ROOT'].'/imag/project/reviews/normal/';
		$to = $_SERVER['DOCUMENT_ROOT'].'/imag/project/reviews/thumb/';
		
		$image->resizeAndSave($from.$name,$to.$name,100,100,true);
		// var_dump($image);
		// echo "<img src='http://g19.local/imag/project/reviews/thumb/".$name."'>";
	}

	private function generateThumbBig($name)
	{
		$image = new Images();
		
		$from = $_SERVER['DOCUMENT_ROOT'].'/imag/project/reviews/normal/';
		$to = $_SERVER['DOCUMENT_ROOT'].'/imag/project/reviews/thumbbig/';
		
		$image->resizeAndSave($from.$name,$to.$name,400,300,true);
		// var_dump($image);
		// echo "<img src='http://g19.local/imag/project/reviews/thumb/".$name."'>";
	}
	
	private function getIdOfReview($friendly)
	{
		$model 	= $this->getClass( 'ProjectReviewModel' );
		$info = $model->getInfoURL($friendly);
	
		//echo "hola1";
		if($info[0]["id_new_url"] == 0)
		{
			return $info[0]["id_review"];
		} 
		else 
		{
			//echo "hola2";
			$url = $model->getURLbyID($info[0]["id_new_url"]);
			//echo $url[0]["url"];
			header('Location: '.URL_ABSOLUTE.'review/'.$url[0]["url"]);
			//return $this->getIdOfReview($info[0]["url"]);
		}
	}
	
	private function checkInfo()
	{	
				
			var_dump(Filter::getRawRequest());	
			if ( Filter::isSent( 'title' )  && Filter::isSent( 'description' )  && Filter::isSent( 'country' ) && Filter::isSent( 'days' ) && Filter::isSent( 'date' ) 
									&& Filter::isSent( 'qualification' ) && Filter::isSent( 'hashtags' ) )
			{
				$error= false;
				$title = Filter::getString( 'title' );
				if ($title == ""){
					$error= true;
					$error_title="Write a Title for the review.";
					$this->assign( 'title_error', 'true' );
					$this->assign( 'title_msg', $error_title );
				}
				else{
					if(strlen($title)>80){
						$error= true;
						$error_title="Write a Title with less than 80 words lenght.";
						$this->assign( 'title_error', 'true' );
						$this->assign( 'title_msg', $error_title );
					}
				}
								
				$description = Filter::getString( 'description' );
				if ($description == ""){
					$error= true;
					$error_description="Write the description of your review.";
					$this->assign( 'description_error', 'true' );
					$this->assign( 'description_msg', $error_description );
				}
				
				$country = Filter::getString( 'country' );
				if ($country == ""){
					$error= true;
					$error_country="Write the name of the country for the review.";
					$this->assign( 'country_error', 'true' );
					$this->assign( 'country_msg', $error_country );
				}

				$days = Filter::getString( 'days' );
				if ($days == "" || $days <= 0){
					$error= true;
					$error_days="Write the number of days for the review.";
					$this->assign( 'days_error', 'true' );
					$this->assign( 'days_msg', $error_days );
				}
				
				
				$date = Filter::getString( 'date' );
				if ($date == ""){
					$error= true;
					$error_date="Write the date of the review.";
					$this->assign( 'date_error', 'true' );
					$this->assign( 'date_msg', $error_date );
				}
				else{
					list($dia, $mes, $año) = split('[/.-]', $date);
					$correctdate=checkdate($mes,$dia,$año);
					if ($correctdate==false){
						$error= true;
						$error_date="Write dd/mm/yyyy for a correct format date.";
						$this->assign( 'date_error', 'true' );
						$this->assign( 'date_msg', $error_date );
					}
				}

				$hashtags = Filter::getString( 'hashtags' );
				if ($hashtags == ""){
					$error= true;
					$error_hashtags="Write at least one hashtag for the review.";
					$this->assign( 'hashtags_error', 'true' );
					$this->assign( 'hashtags_msg', $error_hashtags );
				}		
					
				$qualification = Filter::getString( 'qualification' );
				if ($qualification == ""){
					$error= true;
					$error_qualification="Write a valid qualification.";
					$this->assign( 'qualification_error', 'true' );
					$this->assign( 'qualification_msg', $error_qualification );
				}
				else{
					if ($qualification<1 || $qualification>10){
						$error= true;
						$error_qualification="Write a qualification between 1-10.";
						$this->assign( 'qualification_error', 'true' );
						$this->assign( 'qualification_msg', $error_qualification );
					}
				}
				
				if(Filter::isSent('changeimg'))
				{
					if( $_FILES["imgbig"]["name"] == ""){
						$error= true;
						$error_ri="Select an image to upload.";
						$this->assign( 'ri_error', 'true' );
						$this->assign( 'ri_msg', $error_ri );
					} 
					else
					{
						$filesize = $_FILES["imgbig"]["size"]/1024;
						//echo "error1";
						if($filesize > 2000){
						//	echo "erroe2";
							$error= true;
							$error_ri="Image less than 2MB.";
							$this->assign( 'ri_error', 'true' );
							$this->assign( 'ri_msg', $error_ri );
						}
						else
						{
							$acceptedfiletypes = array('image/jpg','image/jpeg', 'image/gif', 'image/png');
							$filetype = $_FILES['imgbig']['type'];
		
							if(!in_array($filetype, $acceptedfiletypes)){
								$error= true;
								$error_ri="Accepted files: jpg, jpeg, gif, png.";
								$this->assign( 'ri_error', 'true' );
								$this->assign( 'ri_msg', $error_ri );								
							}
						}
					}
				}
					
				if($error === true){
				
					$data['title'] = $title;
					$data['description'] = $description;
					$data['country'] = $country;
					$data['days'] = $days;
					$data['date'] = $date;
					$data['hashtags'] = $hashtags;
					$data['qualification'] = $qualification;	
					$q = 'q'.$qualification.'a';				
					$this->assign($q,'true');
					$this->assign('data',$data);
			
				}
			}
			else
			{
				$error = true;
			}
			return $error;
	}

	
	public function loadModules()
	{
		$modules['header']	= 'ProjectHeaderController';
		$modules['footer']	= 'ProjectFooterController';

		return $modules;
	}
}
?>