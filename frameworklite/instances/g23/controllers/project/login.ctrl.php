<?php
class ProjectLoginController extends Controller
{
	public function build( )
	{
		
		$params = $this->getParams();
		//var_dump($params);		
		
		//switch (@$params['url_arguments'][0])
		//{
		
		if(isset($params['url_arguments'][0]) && $params['url_arguments'][0] == "out")
		{
			//case 'out':
		
				$session =  Session::getInstance();	
				$session->destroy();
				header('Location: '.URL_ABSOLUTE.'home');
		}else{//	break;
			//default:
			
				$this->setLayout("project/login.tpl");
				$this->assign( 'isuser', 'true' );
				if ( Filter::isSent( 'email' ) && Filter::isSent( 'password' ) )
				{
					
					$model 	= $this->getClass( 'ProjectLoginModel' );
					$email = Filter::getString( 'email' );
					$pass=  Filter::getString( 'password' );					
					$iduser = $model->checkUser($email, md5($pass));
					// var_dump($email);
					// var_dump($pass);
					// var_dump( md5($pass));
					// var_dump($iduser);
					//var_dump ($iduser);
					if ( empty ($iduser)){
						
							$this->assign( 'isuser', 'false' );
							$data['email'] = $email;
							$data['pwd'] = $pass;
							$this->assign('data',$data);
					}else{
						
						$user=$model->getUser($iduser[0]['id']);
						//var_dump($user);
						
						$session =  Session::getInstance();	
					
						$session->set($user[0]);
						
						$session->set('logged',true);
				
						header('Location: '.URL_ABSOLUTE.'home');
					}
				}			
		}
	}
}
?>