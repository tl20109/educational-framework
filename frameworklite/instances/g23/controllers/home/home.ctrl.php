<?php
/**
 * Home Controller: Controller example.

 */
class HomeHomeController extends Controller
{
	protected $view = 'home/home.tpl';

	public function build()
	{
        
		$this->setLayout( $this->view );
		$model 	= $this->getClass( 'ProjectHomeModel' );
		$maxQualification=$model->getMaxQualification();
		//var_dump($maxQualification);
		$image=$model->get10Images();
		$review=$model->getLast10Review();
		/*for ($i=0;$i<count($review);$i++){
			
		
			if (strlen($review[$i]['description'])>= 50){
				
				$review[$i]['description'] = substr($review[$i]['description'],0,50)."...";
			}
		}*/
		
		foreach($review as $entry)
		{
			$entry['description'] = substr( strip_tags($entry['description']) ,0,50)."...";
			
			$reviews[] = $entry;
		}
		
		if(!empty($maxQualification[0]))
			$this->assign('maxq',$maxQualification[0]);
		if(!empty($image))
			$this->assign('image10',$image);
		if(!empty($reviews))
			$this->assign('review10',$reviews);
	
	}

	/**
	 * With this method you can load other modules that we will need in our page. You will have these modules availables in your template inside the "modules" array (example: {$modules.head}).
	 * The sintax is the following:
	 * $modules['name_in_the_modules_array_of_Smarty_template'] = Controller_name_to_load;
	 *
	 * @return array
	 */
	public function loadModules() {
		$modules['head']	= 'SharedHeadController';
		$modules['footer']	= 'SharedFooterController';
		return $modules;
	}
}