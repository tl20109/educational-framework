<?php
class ProjectRegisterModel extends Model
{
	public function checkName($name)
	{
	
		$sql = <<<QUERY
SELECT 
	name
FROM
	users
WHERE
	name = '$name'
QUERY;
		return $this->getAll($sql);
	}
	
	public function checkBday($bday)
	{
		$sql = <<<QUERY
SELECT 
	bday
FROM
	users
WHERE
	bday = '$bday'
QUERY;
		return $this->getAll($sql);
	}

	public function checkEmail($email)
	{
		$sql = <<<QUERY
SELECT 
	email
FROM
	users
WHERE
	email = '$email'
QUERY;
		return $this->getAll($sql);	
	}
	
	public function insertUser($data)
	{
		$name = $data['name'];
		$bday = $data['bday'];
		$password = $data['password'];
		$email = $data['email'];
		$hash = $data['hash'];
	
		$sql = <<<QUERY
INSERT INTO
	users
	(name,bday,email,password,hash)
VALUES
	('$name','$bday','$email','$password','$hash')
QUERY;
		return $this->Execute($sql);
	}
	
	//Que torni el valor de l'active si jo li passo un hash
	public function checkActive($hash)
	{
		$sql = <<<QUERY
SELECT
	active
FROM
	users
WHERE
	hash='$hash'
QUERY;
		return $this->getAll($sql);
	}
	//Que canvii l'active de 0 a 1 si jo li passo un hash
	public function changeActive($hash)
	{
		$sql = <<<QUERY
UPDATE 
	users
SET
	active = '1'
WHERE
	hash = '$hash'
QUERY;
		return $this->Execute($sql);
	}
	
	public function getUser($hash)
	{
		$sql = <<<QUERY
SELECT
	id, 
	name,
	bday,
	email
FROM
	users
WHERE
	hash = '$hash'
QUERY;
		return $this->getAll($sql);
	}
}
?>