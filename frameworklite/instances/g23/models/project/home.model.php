<?php
class ProjectHomeModel extends Model
{
	public function get10Images()
	{
		$sql= <<<QUERY
SELECT
		image
FROM
		images
ORDER BY
		id
DESC
LIMIT 10
QUERY;
		return $this->getAll($sql);
	}
	public function getLast10Review()
	{
	$sql= <<<QUERY
SELECT
		*, DATE_FORMAT(reviews.add_date,'%d-%m-%Y') AS fecha
FROM
		reviews, reviews_url, images
WHERE
	reviews.id = reviews_url.id_review
	AND 
	reviews_url.id_new_url = 0
	AND
	reviews.id = images.id_review
	AND 
	images.actual = '0'
ORDER BY
		reviews.id
DESC
LIMIT 10
QUERY;
		return $this->getAll($sql);
	}
}
?>