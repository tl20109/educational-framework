<?php
class ProjectReviewModel extends Model
{

	public function insertReview($data){
		$title = mysql_real_escape_string($data['title']);
		$id_user = $data['id_user'];
		$description = mysql_real_escape_string($data['description']);
		$days = mysql_real_escape_string($data['days']);
		$country = mysql_real_escape_string($data['country']);
		$hashtags = mysql_real_escape_string($data['hashtags']);
		$date = mysql_real_escape_string($data['date']);
		$qualification = mysql_real_escape_string($data['qualification']);
	
		$sql = <<<QUERY
INSERT INTO
	reviews
	(title,id_user,description,days,country,hashtags,date,qualification,add_date)
VALUES
	('$title','$id_user','$description','$days','$country','$hashtags','$date','$qualification',NOW())
QUERY;
		$this->Execute($sql);
		return $this->Insert_ID();
	}
	
	public function updateReview($id,$data)
	{
		$title = mysql_real_escape_string($data['title']);
		$description = mysql_real_escape_string($data['description']);
		$days = mysql_real_escape_string($data['days']);
		$country = mysql_real_escape_string($data['country']);
		$hashtags = mysql_real_escape_string($data['hashtags']);
		$date = mysql_real_escape_string($data['date']);
		$qualification = mysql_real_escape_string($data['qualification']);
		$sql = <<<QUERY
UPDATE
	reviews
SET
	title = '$title',
	description = '$description', 
	days = '$days',
	country = '$country',
	hashtags = '$hashtags', 
	date = '$date', 
	qualification = '$qualification',
	add_date = NOW()
WHERE
	id = '$id'
QUERY;
		$this->Execute($sql);
	}
	
	public function insertURL($url,$id)
	{
		$sql = <<<QUERY
INSERT INTO
	reviews_url
	(url,id_review)
VALUES
	('$url','$id')
QUERY;
		$this->Execute($sql);
		return $this->Insert_ID();
	}
	
	public function updateURL($id_review,$id_nueva)
	{
		$sql = <<<QUERY
UPDATE
	reviews_url
SET
	id_new_url = $id_nueva
WHERE
	id_review = $id_review
	AND
	id != $id_nueva
QUERY;
		$this->Execute($sql);
		return $this->Insert_ID();
	}
	

	public function updateImage($id)
	{
		$sql = <<<QUERY
UPDATE
	images
SET
	actual = '1'
WHERE
	id_review = '$id'
QUERY;
		return $this->Execute($sql);
	}
	
	
	public function insertImage($image,$id)
	{
		$sql = <<<QUERY
INSERT INTO
	images
	(id_review,image)
VALUES
	('$id','$image')
QUERY;
		return $this->Execute($sql);
	}
	
	public function getReview($id){
		$sql = <<<QUERY
SELECT
	*,DATE_FORMAT(reviews.add_date,'%d-%m-%Y') AS fecha
FROM
	reviews, users, images
WHERE
	reviews.id='$id'
	AND
	reviews.id_user = users.id
	AND
	reviews.id = images.id_review
	AND
	images.actual = '0'
QUERY;
		return $this->getAll($sql);
	}
	
	public function getNumberByUser($id)
	{
		$sql = <<<QUERY
SELECT
	COUNT(*) AS total
FROM
	reviews
WHERE
	id_user='$id'
QUERY;
		return $this->getAll($sql);
	}
	
	public function getByUser($id,$start,$max)
	{
		$sql = <<<QUERY
SELECT
	*
FROM
	reviews
WHERE
	id_user='$id'
ORDER BY
	id
DESC
LIMIT $start,$max
QUERY;
		return $this->getAll($sql);
	}

	public function getCountByUser($id)
	{
		$sql = <<<QUERY
SELECT
	COUNT(*) as total
FROM
	reviews
WHERE
	id_user='$id'

QUERY;
		return $this->getAll($sql);
	}
	
	public function isYours($id,$id_user)
	{
		$sql = <<<QUERY
SELECT
	*
FROM
	reviews
WHERE
	id_user='$id_user'
	AND
	id='$id'
QUERY;
		return $this->getAll($sql);
	}
	
	public function getURL($id)
	{
		$sql = <<<QUERY
SELECT
	*
FROM
	reviews_url
WHERE
	id_review='$id'
	AND
	id_new_url = 0
QUERY;
		return $this->getAll($sql);
	}
	
	public function getURLbyID($id)
	{
		$sql = <<<QUERY
SELECT
	*
FROM
	reviews_url
WHERE
	id='$id'
QUERY;
		return $this->getAll($sql);
	}
	
	public function deleteReview($id)
	{
	
		$sql=<<<QUERY
DELETE
FROM
		reviews
WHERE
		id='$id'
QUERY;

		$this->Execute( $sql );
	
	}
	
	public function getInfoURL($friendly)
	{
		$sql = <<<QUERY
SELECT
	*
FROM
	reviews_url
WHERE
	url='$friendly'
	AND
	id_new_url = 0
QUERY;
		return $this->getAll($sql);
	}

	public function searchReview($input)
	{
		$sql = <<<QUERY
SELECT
	*, DATE_FORMAT(reviews.add_date,'%d-%m-%Y') AS fecha
FROM
	reviews, images, reviews_url
WHERE

	reviews.id = reviews_url.id_review
	AND
	images.id_review = reviews.id
	AND
	images.actual = '0'
	AND
	reviews_url.id_new_url = 0
	AND
	(
	hashtags LIKE "%$input%"
	)	
QUERY;
		return $this->getAll($sql);
		
	}

	public function unfollow($id, $sid)
	{
	
		$sql=<<<QUERY
DELETE
FROM
		followers
WHERE
		id_seguido='$id'
		AND
		id_seguidor=$sid
QUERY;

		$this->Execute( $sql );
	
	}

	public function follow($id, $sid)
	{
	
		$sql=<<<QUERY
INSERT 
INTO
		followers
		(id_seguido, id_seguidor)
VALUES
		($id, $sid)

QUERY;

		$this->Execute( $sql );
	
	}

	public function getFollowers($input)
	{
		$sql = <<<QUERY
SELECT
	users.name, users.id
FROM
	followers, users
WHERE
	
	followers.id_seguidor='$input'
	AND
	followers.id_seguido=users.id

QUERY;
		return $this->getAll($sql);
		
	}

	public function searchFollowers($input, $sid)
	{
		$sql = <<<QUERY
SELECT
	users.name, users.id
FROM
	users, reviews
WHERE
	(users.id = reviews.id_user
	AND
	users.id <> $sid
	AND
	reviews.country LIKE "%$input%"
	AND
	NOT EXISTS (SELECT * FROM followers as f
		WHERE f.id_seguido = reviews.id_user
		AND f.id_seguidor = $sid)
	) OR (
		users.name LIKE "%$input%"
		AND
		users.id <> $sid
		AND
		NOT EXISTS (SELECT * FROM followers as fa
		WHERE fa.id_seguido = users.id
		AND fa.id_seguidor = $sid)
	)
	
QUERY;
		return $this->getAll($sql);
		
	}
}
?>