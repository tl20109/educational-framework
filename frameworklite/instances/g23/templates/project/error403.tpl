 {$modules.header}
 
  <title>ERROR 403 - FORBIDDEN </title>
 

  <center> <img src="{$url.global}imag/project/error_403.jpg" width="200" height="200"/> </center>

   <br>
   Most likely causes: <br><br>
	 This website requires a <b>login</b> on him.<br><br>
 
   		You can try the following:<br><br>
  
      	- Return to the previous page.<br><br>
 	
      	- More information.<br><br>

		This error (HTTP 403 Forbidden) means that the browser could connect to the website, but do not have permission to view this website.<br>

 {$modules.footer}