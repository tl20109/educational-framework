{$modules.header}	
 <div class="section">
     <img src="{$url.global}imag/project/section/search.jpg" width="115" height="32" alt="Home" longdesc="Home - La Salle Social Travel" />
</div>
{if $vacio eq 'true'}
	The search field is empty.

{else}

	{if $nohay eq 'true'}
		There is no match found.
	{else}

			{foreach from=$reviews item=review} <!-- comprovar si tenen imatges o no -->
			<p style="clear: both;">
			<img src="{$url.global}imag/project/reviews/thumb/{$review.image}" style="float: left; margin: 0px 5px 5px 0px;" />
			<h3><a href="{$url.global}review/{$review.url}">{$review.title}</a></h3>			<!-- ha de ser un link cap al visionat públic d’aquesta review -->
			{$review.description}<br>		<!-- els 50 primers caracters de la review -->
			<strong>Date:</strong> {$review.date}
			<strong>Qualification:</strong> {$review.qualification}</p>
		{/foreach}
	{/if}
{/if}	
{$modules.footer}