{$modules.header}
	<!---<form enctype="multipart/form-data" action="" method="POST">
 Please choose a file: <input name="imgbig" type="file" /><br />
 <input type="submit" value="Upload" />
 </form> -->
 <script src="{$url.global}js/date.js"></script>
 <link rel="stylesheet" href="{$url.global}js/jwysiwyg/jquery.wysiwyg.css" type="text/css" /> 
 <!--  <script type="text/javascript" src="{$url.global}js/jquery/jquery-1.3.2.js"></script>  -->
  <script type="text/javascript" src="{$url.global}js/jwysiwyg/jquery.wysiwyg.js"></script> 
   <script src="{$url.global}js/editor.js"></script>
 

	<title>Submit - La Salle Social Travel</title>
<div class="section">
     <img src="{$url.global}imag/project/section/submit.jpg" width="93" height="32" alt="Home" longdesc="Home - La Salle Social Travel" />
</div>
 {if $sended eq 'true'}
			 <p>{if $inserted eq 'true'}
			 	Redirigir al post	 	
			 {else}
			 	An Error occurred, please try again later. 
			 {/if}</p>
     {else}   
    		<div id="b_form">
			 	<form enctype="multipart/form-data"  action="" method="post">
			 	
				 	<fieldset>
				 		<legend>Basic Information</legend>
				 		
				 		<label for="title" class="float"><strong>Title:</strong></label><br />
						<input class="inp-text" name="title" id="title" type="text" size="30" value="{$data.title}" />
						{if $title_error eq 'true'}
							<span class="err">{$title_msg}</span>
						{/if}
						<br />
						
						<label for="Country" class="float"><strong>Country:</strong></label><br />
						<input class="inp-text" name="country" id="country" type="text" size="30" value="{$data.country}" />
						{if $country_error eq 'true'}
							<span  class="err">{$country_msg}</span>
						{/if}
						<br />
						
						<label for="Date" class="float"><strong>Date:</strong></label><br />
						<input class="inp-text" name="date" id="datepicker" type="text" size="30" value="{$data.date}" />
						{if $date_error eq 'true'}
							<span  class="err">{$date_msg}</span>
						{/if}
						<br />

						<label for="Days" class="float"><strong>Days:</strong></label><br />
						<input class="inp-text" name="days" id="days" type="text" size="30" value="{$data.days}" />
						{if $days_error eq 'true'}
							<span  class="err">{$days_msg}</span>
						{/if}
						<br />

						<label for="Hashtags" class="float"><strong>Hashtags:</strong></label><br />
						<input class="inp-text" name="hashtags" id="hashtags" type="hidden" value="{$data.hashtags}">
            			<ul id="singleFieldTags"></ul>
           				<div id="events_container" class="err"></div>
						{if $hashtags_error eq 'true'}
							<span  class="err">{$hashtags_msg}</span>
						{/if}
						<br />
												
						<label for="Qualification" class="float"><strong>Qualification:</strong></label><br />
						<select id="qualification" name="qualification">
									{if  $q1a eq 'true' }
								<option value="1" selected="selected"  >1</option>
							{else}
								<option value="1"  >1</option>
							{/if}
							{if  $q2a eq 'true' }
								<option value="2" selected="selected"  >2</option>
							{else}
								<option value="2"  >2</option>
							{/if}
							{if  $q3a eq 'true' }
								<option value="3" selected="selected"  >3</option>
							{else}
								<option value="3"  >3</option>
							{/if}
							{if  $q4a eq 'true' }
								<option value="4" selected="selected"  >4</option>
							{else}
								<option value="4"  >4</option>
							{/if}
							{if  $q5a eq 'true' }
								<option value="5" selected="selected"  >5</option>
							{else}
								<option value="5"  >5</option>
							{/if}
							{if  $q6a eq 'true' }
								<option value="6" selected="selected"  >6</option>
							{else}
								<option value="6"  >6</option>
							{/if}
							{if  $q7a eq 'true' }
								<option value="7" selected="selected"  >7</option>
							{else}
								<option value="7"  >7</option>
							{/if}
							{if  $q8a eq 'true' }
								<option value="8" selected="selected"  >8</option>
							{else}
								<option value="8"  >8</option>
							{/if}
							{if  $q9a eq 'true' }
								<option value="9" selected="selected"  >9</option>
							{else}
								<option value="9"  >9</option>
							{/if}
							{if  $q10a eq 'true' }
								<option value="10" selected="selected"  >10</option>
							{else}
								<option value="10"  >10</option>
							{/if}
							
						</select>
						{if $qualification_error eq 'true'}
							<span  class="err">{$qualification_msg}</span>
						{/if}
						<br />
						
						<label for="Image Big" class="float"><strong>Image:</strong></label><br />
						<input class="files" type="file" name="imgbig">
						{if $ri_error eq 'true'}
							<span  class="err">{$ri_msg}</span>
						{/if}
						<br /> 
						
						<input type="checkbox"  name="changeimg" checked="checked" style="display:none;" />
				 	</fieldset>
				 	<fieldset>
				 	    <legend>Description</legend>
				 		<textarea name="description" id="description" cols="30" rows="5"  >{$data.description}</textarea>
						{if $description_error eq 'true'}
							<span  class="err">{$description_msg}</span>
						{/if}
				 	</fieldset>
				 	<p><input type="submit" value="enviar" /></p>
				 </form>
			 </div>
	{/if}
	
	
 {$modules.footer}