{$modules.header}
<div class="section">
     <img src="{$url.global}imag/project/section/view.jpg" width="93" height="32" alt="Home" longdesc="Home - La Salle Social Travel" />
</div>
{if $found eq 'true'}
	<!--{$data.id}<br>
	{$data.id_user}<br>-->
	<h2>{$data.title}</h2>
	
	<strong>Hashtags: </strong> 
	{foreach from=$data.hashtags item=hashtag} 
	<a href="{$url.global}review/hash/{$hashtag}">#{$hashtag}</a>&nbsp;
	{/foreach}
	<br>
	<strong>Date: </strong> {$data.date} <strong>Days:</strong> {$data.days} <br> 
	<strong>Author: </strong>{$data.name} ({$data.total}) <br> <strong>Qualification:</strong> {$data.qualification}
	<br><br><br>
	<p>{$data.description}</p>			
	<center><img src="{$url.global}imag/project/reviews/thumbbig/{$data.image}" width="400" height="300" /></center>
	
	<br>
{else}
	Does not exists that review in our database.
{/if}

{$modules.footer}