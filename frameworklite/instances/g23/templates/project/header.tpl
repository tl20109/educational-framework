<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<meta name="title" content="" />
	<meta name="robots" content="all" />
	
	<meta name="expires" content="never" />
	<meta name="distribution" content="world" />


<link href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css" rel="stylesheet" type="text/css"/>
  <script src="http://code.jquery.com/jquery-1.10.2.js"></script>
  <script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
  <link href="{$url.global}css/jquery.tagit.css" rel="stylesheet" type="text/css">
    <link href="{$url.global}css/tagit.ui-zendesk.css" rel="stylesheet" type="text/css">
    <!-- The real deal -->
    <script src="{$url.global}js/tag-it.js" type="text/javascript" charset="utf-8"></script>
     <script src="{$url.global}js/tags.js" type="text/javascript" charset="utf-8"></script>
  <link rel="stylesheet" type="text/css" media="all" href="{$url.global}css/project2.css">
<!-- <title>LaSalle Social Travel</title> --> 
</head>

<body>
<div id="header">
    	<div class="logo">
   			<img src="{$url.global}imag/project/logo.jpg" alt="La Salle Social Travel" longdesc="La Salle Social Travel" />
    	</div>
        <div class="login">
	        {if $logged eq 'true'}
	    		<a href="{$url.global}login/out">Logout [ {$name} ]</a>
	    	{else}
		        	<p><a href="{$url.global}login">Login</a>&nbsp;|&nbsp;<a href="{$url.global}register">Register</a></p>
		            
		            <form action="{$url.global}login" method="post">
		                <input class="in_email"  type="text" name="email" />
		                <input class="in_pwd"  type="password" name="password" />
		              <input class="in_go" value="" type="submit" name="name" />
		          	</form>
		     {/if}
        </div>
    </div>
	<div id="bar">
    	<div id="menu">
 			<a href="{$url.global}"><img border="0" src="{$url.global}imag/project/menu/home_off.jpg" width="92" height="25" alt="Home" longdesc="Home - La Salle Social Travel" /></a>       		
    		<a href="{$url.global}review/add"><img border="0" src="{$url.global}imag/project/menu/submit_off.jpg" width="78" height="25" alt="Submit" longdesc="Submit - La Salle Social Travel" /></a>
           	<a href="{$url.global}review/manage"> <img border="0" src="{$url.global}imag/project/menu/manage_off.jpg" width="79" height="25" alt="Manage" longdesc="Manage - La Salle Social Travel" /></a>
            <a href="{$url.global}review/follow"> <img border="0" src="{$url.global}imag/project/menu/follow_off.jpg" width="64" height="25" alt="Follow" longdesc="Follow - La Salle Social Travel" /></a>
  			
  			    <form style="float: right;" action="{$url.global}search" method="post">
              <input type="text" name="search" />
           		<input value="search" type="submit" name="submit" />
          	</form>
         </div>
    </div>
    <div id="body_wrapper">