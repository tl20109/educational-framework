{$modules.header}
<title>Home - LaSalle Social Travel</title>
<div class="section">
     <img src="{$url.global}imag/project/section/home.jpg" width="93" height="32" alt="Home" longdesc="Home - LaSalle Social Travel" />
</div>

	<h1>Last Review</h1>
	
	<p><h2><a href="{$url.global}review/{$review10[0].url}">{$review10[0].title}</a></h2>
	<strong>Hashtags:</strong>
	{foreach from=$review10[0].hashtags item=hashtag} 
	<a href="{$url.global}review/hash/{$hashtag}">#{$hashtag}</a>&nbsp;
	{/foreach}
	<br><br><br>
	<center><img src="{$url.global}imag/project/reviews/normal/{$review10[0].image}"></center>
		
	<h1>Latest 10 Reviews</h1>
	{foreach from=$review10 item=review} <!-- comprovar si te imatges o no -->
	<p style="clear: both;">
		<br><!-- ha de ser un link cap al visionat públic d’aquesta review -->
		<img src="{$url.global}imag/project/reviews/thumb/{$review.image}" style="float: left; margin: 0px 5px 5px 0px;" />
		<h3><a href="{$url.global}review/{$review.url}">{$review.title}</a></h3>			<!-- ha de ser un link cap al visionat públic d’aquesta review -->
		{$review.description}<br>		<!-- els 50 primers caracters de la review -->
		<strong>Date:</strong> {$review.date}
		<strong>Qualification:</strong> {$review.qualification}</p>
	{/foreach}
	<br><br><br>
		
	<h1>Latest 10 Images</h1>
	<div style=" width:600px; margin: 0 auto">
	{foreach from=$image10 item=image} <!-- comprovar si te imatges o no -->
		<img src="{$url.global}imag/project/reviews/thumb/{$image.image}" width="100" height="100" />
	{/foreach}
	</div>
	
{$modules.footer}