{$modules.header}
<div class="section">
     <img src="{$url.global}imag/project/section/activation.jpg" width="220" height="31" alt="Home" longdesc="Home - La Salle Reviews" />
</div>
	{if $ValidHash eq 'true'}
		{if $isActivated eq 'true'}
			<p>This account has been already activated.</p>
		{else}
			<p>Welcome to La Salle Social Travel Aplication</p>
		
			<p>Your account has been activated successfully and you has been logged, in a few seconds you will be redirected automatically.
			If your browser do not redirect you, <a href="{$url.global}home">click here</a>.</p>
		{/if}
	{else}
		<p>Current activation link is not assigned to any account in our database.</p>
	{/if}
{$modules.footer}