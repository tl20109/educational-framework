 {$modules.header}
 <script src="{$url.global}js/date.js"></script>


 <title>Register - LaSalle Social Travel</title>
		
		<div class="section">
     <img src="{$url.global}imag/project/section/register.jpg" width="93" height="32" alt="Home" longdesc="Home - La Salle Social Travel" />
</div>
	{if $sended eq 'true'}
			 <p>{if $inserted eq 'true'}
			 	{if $dev_mode eq 'false'}
			 		MODE ONLINE<br />
			 		To activate account : <a href="{$link}">{$link}</a>
			 		
			 	{else}
			 		An email has been sent to you current email address with the activation link of your account.
			 	{/if}	 	
			 {else}
			 	An Error occurred, please try again later. 
			 {/if}</p>
     {else}   
   			 
		<div id="b_form"> 
			  <form action="" method="post">
				  <fieldset>
						<legend>Register New Member</legend>
							<label for="name" class="float"><strong>Name:</strong></label><br />
							<input class="inp-text" name="name" id="name" type="text" size="30" value="{$data.name}" />
							{if $name_error eq 'true'}
								<span  class="err">{$name_msg}</span>
							{/if}
							<br />
			
							<label for="bday" class="float"><strong>Bday:</strong></label><br />
							<input class="inp-text" name="bday"  id="datepicker" type="text" size="30" value="{$data.bday}"/>
							{if $bday_error eq 'true'}
								<span  class="err">{$bday_msg}</span>
							{/if}
							<br />
							
							<label for="email" class="float"><strong>eMail</strong></label><br />
							<input class="inp-text" name="email"  id="email" type="text" size="30" value="{$data.email}"/>
							{if $email_error eq 'true'}
								<span  class="err">{$email_msg}</span>
							{/if}
							<br />
							
							<label for="password" class="float"><strong>Password</strong></label><br />
							<input class="inp-text" name="password"  id="password" type="password" size="30" value="{$data.pwd}"/>
				  			{if $pass_error eq 'true'}
								<span  class="err">{$pass_msg}</span>
							{/if}
							<br />
				  </fieldset>
				  <p><input class="submit-button" type="submit" alt="Register" name="Submit" value="" /></p>
			  </form>
		</div>	 
			 
	{/if}

 
 {$modules.footer}