<?php
/**
	$u = new Uploader();

	//LOOP
	$u->setInitVars("imag/","image.jpg");
	$u->upload();
	$filename = $u->getFileName();
	//END LOOP

*/
class Uploader
{
	private $path;
	private $field_name;

	function __construct()
	{}

	public function upload()
	{
		$target_path = $this->path . basename( $_FILES[ $this->field_name ]['name'] );

		if ( move_uploaded_file( $_FILES[ $this->field_name ]['tmp_name'], $target_path ) )
		{
			chmod( $target_path, 0755);
			return true;
		}
		else
		{
			return false;
		}

	}

	public function setInitVars( $path_name, $field_name )
	{
		$this->path 		= $path_name;
		$this->field_name 	= $field_name;
	}

	public function getFileName()
	{
		return $_FILES[ $this->field_name ]['name'];
	}

}