<?php

class Db
{
	static private $adodb = false;
	static private $instance = NULL;

	public static $debug;

	static public function getInstance()
	{
		if ( self::$instance === null )
		{
			self::$instance = new Db(); //create class instance
			include( PATH_ENGINE . 'adodb5/adodb-exceptions.inc.php' ); //include exceptions for php5
			include( PATH_ENGINE . 'adodb5/adodb.inc.php' );

			$profiles 	= Configure::getInstance( 'db' );
			$db_params 	= $profiles->get( 'default' );

			self::$adodb 	= NewADOConnection( $db_params['db_driver'] );
			try{
				self::$adodb->Connect( $db_params['db_host'], $db_params['db_user'],$db_params['db_password'], $db_params['db_name'] ); //connect to database constants are taken from config
				self::$adodb->Execute( "set names 'utf8'" );
			}
			catch( Exception $e )
			{
				die( $e->getMessage() );
			}

			$ADODB_FETCH_MODE = ADODB_FETCH_ASSOC;
		}
		return  self::$instance;
	}

	function __call($method, $args)//call adodb methods
	{
		if($method=='init') return;
			
		try
		{
			try
			{
				$answer = call_user_func_array(array(self::$adodb, $method),$args);
			}
			catch( ADODB_Exception $e )
			{
				$answer = false;
				$error = $e->getMessage();
			}

			if ( 'GetRow' == $method || 'GetOne' == $method )
				$resultset = array( $answer );
			else
				$resultset = $answer;

			if ( self::$adodb )
			{
				$debug_query = array(
					"tag" => 'Executed in: '.get_class( $this ),
					"sql" => self::$adodb->_querySQL,
					"type" => ( ( 0 === stripos( self::$adodb->_querySQL, 'SELECT' ) ) ? 'read' : 'write' ),
					"host" => self::$adodb->host,
					"database" => self::$adodb->database,
					"user" => self::$adodb->user,
					"controller" => get_class( $this ),
					"resultset" => $resultset,
					"error" => ( isset( $error ) ? $error : false )
				);
				if ( $debug_query['type'] == 'read' )
				{
					$debug_query['rows_num'] = count( $resultset );
				}
				else
				{
					$debug_query['rows_num'] = self::$adodb->Affected_Rows();
				}

				self::$debug[] = $debug_query;
			}

			return $answer;
		}
		catch( Exception $e )
		{
			if ( DEV_MODE )
			{
				echo $e->getMessage();
			}
		}
	}

	function __get($property)
	{
		return self::$adodb->$property;
	}

	function __set($property, $value)
	{
		self::$adodb[$property] = $value;
	}

	private function __clone()
	{
	}

}
?>
