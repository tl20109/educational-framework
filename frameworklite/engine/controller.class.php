<?php

abstract class Controller
{
	private 	$view;
	protected 	$user;
	protected	$modules;
	protected 	$data_response;
	protected 	$params = array();


	abstract public function build();

	public function execute()
	{
		// Create View
		$this->view = new View;

		// Execute build method.
		$this->data_response = $this->build();

		// Load modules.
		$this->modules = $this->loadModules();

		if( is_array( $this->modules ) )
		{
			// Execute all defined modules.
			$this->executeModules( $this->modules );
		}
	}

	protected function executeModules( $modules )
	{
		foreach( $modules as $name => $controller )
		{
			$module 				= $this->getClass( $controller );
			$module->setParams( $this->params );
			$module->execute();
			$view_modules[ $name ] 	= $module->fetch();
		}

		$this->assign( 'modules', $view_modules );
	}

	/**
	 * Build the debug information. Here it gets all the information and it shows the html debug.
	 *
	 */
	public function buildDebug()
	{
		if ( isset( $_SESSION['debug'] ) && $_SESSION['debug'] )
		{
			$debug_controller = $this->getClass( 'DebugDebugController' );
			$debug_controller->execute();
			$debug_html = $debug_controller->show();
		}
	}

	public function loadModules()
	{
		return false;
	}

	protected function assign( $name, $value )
	{
		$this->view->assign( $name, $value );
	}

	protected function setLayout( $template )
	{
		$this->view->setLayout( $template );
	}

	public function show()
	{
		$this->view->show();
	}

	public function fetch()
	{
		return $this->view->fetch();
	}

	public function getParam( $name )
	{
		if ( isset( $this->params[ $name ] ) )
		{
			return $this->params[ $name ];
		}
		else
		{
			return null;
		}
	}

	public function getParams()
	{
		return $this->params;
	}

	public function setParams( $params )
	{
		$this->params = array_merge( $this->params, $params );
	}

	protected function getClass( $class )
	{
		return Configure::getClass( $class );
	}

	protected function getConfig( $name, $value = null )
	{
		$config 	= Configure::getInstance( $name );

		if ( $value )
		{
			return $config->get( $value );
		}
		else
		{
			return $config->getAll();
		}
	}
}


?>
