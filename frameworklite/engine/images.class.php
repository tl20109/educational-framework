<?php
class Images
{
	/**
	 * Calls the PHPmailer methods.
	 *
	 * @param string $method
	 * @param mixed $args
	 * @return mixed
	 */
	function __call( $method, $args )
	{
		return call_user_func_array( array( $this->images, $method ), $args );
	}

	/**
	 * Resize an image.
	 *
	 * @param file $from
	 * @param file $to
	 * @param integer $width
	 * @param integer $height
	 * @param boolean $crop
	 * @return boolean
	 */
	public function resizeAndSave( $from, $to, $width, $height, $crop = false )
	{
		$fileinfo = pathinfo( $to );
		include_once PATH_ENGINE . 'PhpThumb/ThumbLib.inc.php';

		$thumb = PhpThumbFactory::create($from);

		if ( false === $crop )
		{
			$thumb->resize( $width, $height );
		}
		else
		{
			$thumb->adaptiveResize( $width, $height );
		}

		switch( strtolower( $fileinfo['extension'] ) )
		{
			case 'jpg':
			case 'jpeg':
			{
				$format = 'JPG';
			}break;

			case 'gif':
			{
				$format = 'GIF';
			}break;

			case 'png':
			{
				$format = 'PNG';
			}break;
		}

		if ( $thumb->getFormat() == $format )
		{
			$thumb->save( $to );
		}
		else
		{
			$thumb->save( $to, $format );
		}

		return true;
	}

	/**
	 * Upload and resize an image.
	 *
	 * @param file $from
	 * @param file $to
	 * @param integer $width
	 * @param integer $height
	 * @param boolean $crop
	 * @return boolean
	 */
	public function uploadResizeAndSave( $post_file, $destination, $width, $height, $crop = false )
	{
		try{
		$old_name = $post_file['tmp_name'];
		$upload_info = pathinfo( $old_name );
		$new_name = $upload_info['dirname'].'/'.$post_file['name'];

		move_uploaded_file( $old_name, $new_name );

		self::resizeAndSave( $new_name, $destination, $width, $height, $crop );
		}
		catch( Exception $e)
		{
			echo $e->getMessage();die;
		}
		return true;
	}
}