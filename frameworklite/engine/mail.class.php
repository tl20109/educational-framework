<?php

include( PATH_ENGINE . 'mailer/class.phpmailer.php' );

class Mail
{
	private $mail;

	public function iniMail( $mail_config )
	{
		$this->mail = new phpmailer();

		$this->mail->PluginDir 	= PATH_ENGINE . 'mailer/';
		$this->mail->Mailer 	= $mail_config['mailer_protocol'];
		$this->mail->Host 		= $mail_config['mailer_host'];
		$this->mail->SMTPAuth 	= $mail_config['smtp_authoritation'];
		$this->mail->From 		= $mail_config['mailer_from'];
		$this->mail->FromName 	= $mail_config['mailer_from_name'];

	}

	public function sendMail( $direcciones, $subject, $body )
	{
		$error = '';

		$this->mail->Subject 		= $subject;
		$this->mail->Body 		= $body;
		$this->mail->AltBody 		= $body;

		$this->mail->Timeout		= 120;

		reset( $direcciones );

		while ( list( $clave, $valor ) = each( $direcciones ) )
		{
			$this->mail->AddAddress($valor);

			$exito = $this->mail->Send();

			$intentos=1;
			while((!$exito)&&($intentos<5)&&($this->mail->ErrorInfo!="SMTP Error: Data not accepted"))
			{
				sleep(5);
				//echo $this->mail->ErrorInfo;
				$exito = $this->mail->Send();
				$intentos=$intentos+1;
			}

			if ($this->mail->ErrorInfo=="SMTP Error: Data not accepted")
			{
				$exito=true;
			}
			if ( !$exito )
			{
				$error.= "Problemas enviando correo electrónico a: ".$valor.".<br>".$this->mail->ErrorInfo."<br>";
			}

			// Borro las direcciones de destino establecidas anteriormente
			$this->mail->ClearAddresses();
		}
		return $error;
	}
}
?>