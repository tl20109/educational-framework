<?php

class Url
{
	private $urls;

	function __construct()
	{
		$config 		= Configure::getInstance( 'dispatcher' );
		$this->urls		= array_flip( $config->getConfig() );
	}

	public function buildUrl( $controller, $params = '' )
	{
		$url_base		= URL_ABSOLUTE . '?m=' . $this->urls[ $controller ];

		if( is_array( $params ))
		{
			foreach( $params as $key => $value )
			{
				$request[]	= $key . '=' . $value;
			}

			$url_base .= implode( '&' , $request );
		}

		return $url_base;
	}

	static public function sanitizeString( $string )
	{
		$translate	= array(
			'!' => '-',
			'�' => '-',
			'- ' => '-',
			' ' => '-',
			"'" => '',
			'_' => '-',
			'(' => '-',
			')' => '',
			',' => '-',
			'%' => '%',
			'&' => '-',
			'+' => '-',
			'@' => '-',
			'�' => '-',
			';' => '-',
			'?' => '-',
			'>' => '-',
			'<' => '-',
			'[' => '-',
			']' => '-',
			'�' => '-',
			'`' => '-',
			'$' => '',
			'^' => '',
			'#' => '-',
			'*' => '-',
			'|' => '-',
			'?' => '',
			'·'	=> '',
			'á' => 'a',
			'à' => 'a',
			'é' => 'e',
			'è' => 'e',
			'í' => 'i',
			'ì' => 'i',
			'ó' => 'o',
			'ò' => 'o',
			'ú' => 'u',
			'ù' => 'u',
			'ü' => 'u',
			'ç' => 'c',
			'ñ' => 'n',
			'ï' => 'i',
		);

		foreach( $translate as $k => $v )
		{
			$t[ utf8_decode( $k ) ] = utf8_decode( $v );
		}

		$string = strtr( trim( strtolower( utf8_decode( $string ) ) ), $t );
		$string = str_replace( '--', '-', $string );
		$string = str_replace( '--', '-', $string );

		// Extract the possibles '-' at the end and beginning of the path.
		$string = trim( $string, '-' );

		return utf8_encode( urlencode( $string ) );
	}

}


?>