<?php
/**
 * Filters the request array checking that the values accomplish the given filters.
 *
 * It DOES NOT modify the original value. Use SANITIZE filters for that purpose.
 *
 * @see http://php.net/manual/en/filter.filters.validate.php
 */
class Filter
{
	/**
	 * Regular expression for email validation.
	 */
	const VALID_EMAIL_REGEXP = '/^[A-Z0-9._%\-+]+@(?:[A-Z0-9\-]+\.)+(?:[A-Z]{2}|com|edu|org|net|biz|info|name|aero|biz|info|jobs|travel|museum|name|cat|asia|coop|jobs|mobi|tel|pro|arpa|gov|mil)$/i';

	static private $instance;

	static private $request;

	private function __construct( $request )
	{
		self::$request = &$request;
	}

	/**
	 * Singleton for filtering.
	 *
	 * @return Filter
	 */
	public static function getInstance()
	{
		if ( !self::$instance )
		{
			// TODO: Only parameters by POST are handled, SEO url is the way. Create specific filter for get.
			self::$instance = new self ( array_merge( $_POST, $_GET ) );
			$_POST 	= array();
			$_GET 	= array();
		}
		return self::$instance;
	}

	/**
	 * Checks if a var has been sent in the request.
	 *
	 * @param string $var_name
	 */
	public function isSent( $var_name )
	{
		return isset( self::$request[$var_name] );
	}

	/**
	 * Returns the number of variables found in the post.
	 *
	 * @return integer
	 */
	public function countVars()
	{
		return count( self::$request );
	}


	/**
	 * Returns a string using the FILTER_DEFAULT.
	 *
	 * @param string $var_name
	 * @return string
	 */
	public static function getString( $var_name )
	{
		if ( !isset( self::$request[$var_name] ) )
		{
			return false;
		}

		return filter_var( self::$request[$var_name], FILTER_DEFAULT );
	}

	/**
	 * Get a variable without any type of filtering.
	 *
	 * @param string $var_name
	 * @return string
	 */
	public static function getUnfiltered( $var_name )
	{
		if ( !isset( self::$request[$var_name] ) )
		{
			return false;
		}

		return self::$request[$var_name];
	}

	/**
	 * Returns an email if filtered or false if it is not valid.
	 *
	 * @param string $var_name Request containing the variable.
	 * @return string
	 */
	public static function getEmail( $var_name )
	{
		if ( !isset( self::$request[$var_name] ) )
		{
			return false;
		}

		if ( preg_match( self::VALID_EMAIL_REGEXP, self::$request[$var_name] ) )
		{
			return self::$request[$var_name];
		}

		return false;
		// Vulnerable: return filter_var( self::$request[$var_name], FILTER_VALIDATE_EMAIL );
	}

	/**
	 * Returns if a value might be considered as boolean
	 *
	 * @param string $var_name
	 * @return boolean
	 */
	public static function getBoolean( $var_name )
	{
		if ( !isset( self::$request[$var_name] ) )
		{
			return false;
		}

		return filter_var( self::$request[$var_name], FILTER_VALIDATE_BOOLEAN );
	}

	/**
	 * Returns a float value for the given var.
	 *
	 * @param string $var_name
	 * @param boolean $decimal
	 * @return float
	 */
	public static function getFloat( $var_name, $decimal = null )
	{
		if ( !isset( self::$request[$var_name] ) )
		{
			return false;
		}

		if ( isset( $decimal ) )
		{
			$decimal = array( 'options' => array( 'decimal' => $decimal ) );
		}

		return filter_var( self::$request[$var_name], FILTER_VALIDATE_FLOAT, $decimal );
	}

	/**
	 * Returns the integer value of the var or false.
	 *
	 * @param string $var_name
	 * @param boolean $decimal
	 * @return integer
	 */
	public static function getInteger( $var_name, $min_range = null, $max_range = null )
	{
		if ( !isset( self::$request[$var_name] ) )
		{
			return false;
		}

		$options = null;

		if ( isset( $min_range ) )
		{
			$options['options']['min_range'] = $min_range;
		}

		if ( isset( $max_range ) )
		{
			$options['options']['max_range'] = $max_range;
		}

		return filter_var( self::$request[$var_name], FILTER_VALIDATE_INT, $options );
	}

	/**
	 * Returns the IP value of the var or false.
	 *
	 * @param string $var_name
	 * @param boolean $decimal
	 * @return integer
	 */
	public static function getIP( $var_name, $min_range = null, $max_range = null )
	{
		if ( !isset( self::$request[$var_name] ) )
		{
			return false;
		}

		// Allow IPv4 Ips.
		$options['flags'] = FILTER_FLAG_IPV4;

		if ( isset( $min_range ) )
		{
			$options['options']['min_range'] = $min_range;
		}

		if ( isset( $max_range ) )
		{
			$options['options']['max_range'] = $max_range;
		}

		return filter_var( self::$request[$var_name], FILTER_VALIDATE_IP, $options );
	}

	public static function getRegexp( $var_name, $regexp )
	{
		if ( !isset( self::$request[$var_name] ) )
		{
			return false;
		}

		return filter_var( self::$request[$var_name], FILTER_VALIDATE_REGEXP, array( 'options' => array( 'regexp' => $regexp ) ) );
	}

	public static function getUrl( $var_name )
	{
		if ( !isset( self::$request[$var_name] ) )
		{
			return false;
		}

		$options['options']['flags'] = FILTER_FLAG_PATH_REQUIRED;
		return filter_var( self::$request[$var_name], FILTER_VALIDATE_URL, $options );
	}

	/**
	 * Returns an array on the post UNFILTERED.
	 *
	 * @param unknown_type $var_name
	 * @return unknown
	 */
	public static function getArray( $var_name, $filter_function = null )
	{
		if ( !isset( self::$request[$var_name] ) || !is_array( self::$request[$var_name] ) )
		{
			return false;
		}

		// Returns an unfiltered Array
		if ( null == $filter_function )
		{
			return self::$request[$var_name];
		}

		/*
		TODO: Filter arrays, but filter must be independent of the request.
		foreach ( self::$request[$var_name] as $key => $value )
		{
			$params = func_get_args();
			unset( $params[0] );

			// Prepend the value to the beginning of the array:
			array_unshift( $params, $value );

			$filtered_array[$key] = call_user_func_array( array( $this, $filter_function ), $params );
		}

		return $filtered_array;
		*/

	}

	/**
	 * Get the raw request array as it was received, unfiltered.
	 *
	 * @return array
	 */
	public static function getRawRequest()
	{
		return self::$request;
	}
}
?>