<?php

class User
{
	protected 	$user;

	function __construct()
	{
		// Recovery session
		$session = Factory::getClass( 'session' );
		$this->user = $session->get( 'user_data' );
	}

	public function validUser( )
	{
		return $this->user['status'];
	}

	public function getRole( )
	{
		$decimal_role = base_convert( substr( $this->user['permisos'], 0, 4 ), 2, 10 );

		$config = Configure::getInstance( 'users' );
		$roles	= $config->get( 'roles' );

		return array_search( $decimal_role, $roles );
	}

	public function getDecimalRole( )
	{
		$decimal_role = base_convert( substr( $this->user['permisos'], 0, 4 ), 2, 10 );

		return $decimal_role;
	}

	public function isRole( $role )
	{
		$config = Configure::getInstance( 'users' );
		$roles	= $config->get( 'roles' );

		$user_role 	= $this->getDecimalRole();

		if( isset( $roles[ $role ]) )
		{
			if ( (int)$roles[ $role ] !== (int)$user_role )
			{
				// Redirect Home
				header( 'Location: ' . URL_ABSOLUTE . '?m=home' );
				die;
			}
			return true;
		}
		else
		{
			trigger_error( 'Incorrect Role: $role' );
			return false;
		}
	}

	public function get( $name )
	{
		if( isset( $this->user[$name]) )
		{
			return $this->user[$name];
		}
		else
		{
			return false;
		}

	}
}


?>